from django.urls import path, include
from rest_framework.authtoken import views as token_views
from . import views

urlpatterns = [
    path('api/tasks/', views.TasksViewSet.as_view(), name='all_tasks'),
    path('api/add_task/', views.CreateTasksView.as_view(), name='create_task'),
    path('api/tasks/<int:pk>', views.DestroyTask.as_view(), name='delete_task'),
    path('api/tasks/<int:pk>/edit', views.UpdateTasksView.as_view(), name='update_task'),
    path('api/tasks/history', views.HistoryTasksView.as_view(), ),
    path('api/user/create', views.UserCreate.as_view()),

    # DRF
    path('api-auth/', include('rest_framework.urls')),
    path('api-token-auth/', token_views.obtain_auth_token)
]
