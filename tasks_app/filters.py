from django_filters import rest_framework as filters
from .models import Tasks


class TasksFilter(filters.FilterSet):
    id = filters.NumberFilter(field_name='id')
    name = filters.CharFilter(field_name='name', lookup_expr='icontains')
    description = filters.CharFilter(field_name='description', lookup_expr='icontains')
    status = filters.CharFilter(field_name='status', lookup_expr='icontains')
    assigned_user = filters.CharFilter(field_name='assigned_user__username', lookup_expr='icontains')

    class Meta:
        model = Tasks
        fields = ['id', 'name', 'description', 'status', 'assigned_user']


class HistoryTasksFilter(TasksFilter):
    # filtr do daty w historii zmian
    history_date = filters.DateFilter(method='filter_history_date')

    class Meta:
        model = Tasks
        fields = ['history_date']
