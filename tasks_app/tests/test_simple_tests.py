from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from tasks_app.models import Tasks


class TaskViewSetTests(APITestCase):
    def setUp(self):
        self.tasks_data = {'name': 'Test_Task', 'description': 'Description', 'status': 'Nowy'}
        self.task = Tasks.objects.create(**self.tasks_data)

    def test_get_all_tasks(self):
        response = self.client.get(reverse('all_tasks'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_task(self):
        response = self.client.get(reverse('create_task'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_task(self):
        response = self.client.post(reverse('create_task'), data=self.tasks_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Tasks.objects.count(), 2)

    def test_update_task(self):
        update_data = {'name': 'Updated Task', 'description': 'Updated Description', 'status': 'W toku'}
        response = self.client.put(reverse('update_task', args=[self.task.id]), data=update_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.task.refresh_from_db()
        self.assertEqual(self.task.name, 'Updated Task')
        self.assertEqual(self.task.status, 'W toku')

    def test_delete_task(self):
        response = self.client.delete(reverse('delete_task', args=[self.task.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Tasks.objects.count(), 0)
