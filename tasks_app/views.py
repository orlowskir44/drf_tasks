from django.contrib.auth.models import User
from rest_framework import generics
from django_filters import rest_framework as filters
from simple_history.models import HistoricalRecords
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny
from .models import Tasks
from .serializers import TasksSerializer, HistorySerializer, UserSerializer
from .filters import TasksFilter, HistoryTasksFilter


class TasksViewSet(generics.ListAPIView):
    queryset = Tasks.objects.all()
    serializer_class = TasksSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = TasksFilter


class CreateTasksView(generics.ListCreateAPIView):
    queryset = Tasks.objects.all()
    serializer_class = TasksSerializer
    # permission_classes = [(IsAuthenticated & ReadOnly) | IsAdminUser]


class UpdateTasksView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Tasks.objects.all()
    serializer_class = TasksSerializer


class HistoryTasksView(generics.ListAPIView):
    queryset = Tasks.objects.all()
    serializer_class = HistorySerializer
    history = HistoricalRecords()
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = HistoryTasksFilter


class DestroyTask(generics.RetrieveDestroyAPIView):
    queryset = Tasks.objects.all()
    serializer_class = TasksSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class UserCreate(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
