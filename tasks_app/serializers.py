from rest_framework import serializers
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from .models import Tasks


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'email')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {'input_type': 'password'}
            }
        }

    def create(self, validated_data):
        user = User(email=validated_data['email'], username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        token = Token.objects.create(user=user)  # new
        return user


class TasksSerializer(serializers.ModelSerializer):
    assigned_user_name = serializers.ReadOnlyField(source='assigned_user.username')

    class Meta:
        model = Tasks

        fields = ['id', 'name', 'description', 'status', 'assigned_user', 'assigned_user_name']


class TaskHistorySerializer(TasksSerializer):
    modified_at = serializers.DateTimeField(source='history_date', format='%Y-%m-%d %H:%M:%S')

    class Meta:
        model = Tasks
        fields = ['id', 'description', 'status', 'assigned_user_name', 'modified_at']


class HistorySerializer(serializers.ModelSerializer):
    history = TaskHistorySerializer(many=True, read_only=True)

    class Meta:
        model = Tasks
        fields = ['id', 'history']
