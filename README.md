#  DJANGO Task Management
back-end application in Python, Django, DRF and Postgresql technology, which aims to manage tasks.

## 💡 TECHNOLOGIES
![PYTHON](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)
![DJANGO](https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=white)


## Installation 💾
```
https://gitlab.com/orlowskir44/drf_tasks.git

cd tasks

pip install -r requirements.txt

python manage.py migrate

python manage.py createsuperuser

python manage.py runserver

```

# DATABASE CONFIG:
db name: gfg
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gfg',
        'USER': 'postgres',
        'PASSWORD': 'admin',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
```

## Tests
```
python manage.py test
```

## CURL

### Create token:
```
python manage.py drf_create_token <username>
```
### Get all tasks
```
curl -H "Authorization: <token>" http://127.0.0.1:8000/api/tasks/
```
### Create task
```
curl -X POST -H "Authorization: Token <token>" -d "name=NoweZadanie&description=OpisZadania&status=Nowy" http://127.0.0.1:8000/api/add_task/
```
### Update task
```
curl -X PUT -H "Authorization: Token <token>" -d "name=Zadanie_3" http://127.0.0.1:8000/api/tasks/<id>/edit
```
### Tasks history
```
curl -H "Authorization: <token>" http://127.0.0.1:8000/api/tasks/history
```
### Delete task:
```
curl -X DELETE -H "Authorization: Token <token>" http://127.0.0.1:8000/api/tasks/<id>/edit
```